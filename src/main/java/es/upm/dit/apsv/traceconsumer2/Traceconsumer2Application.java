package es.upm.dit.apsv.traceconsumer2;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import es.upm.dit.apsv.traceconsumer2.Repository.TraceRepository;
import es.upm.dit.apsv.traceconsumer2.Repository.TransportationOrderRepository;
import es.upm.dit.apsv.traceconsumer2.model.Trace;
import es.upm.dit.apsv.traceconsumer2.model.TransportationOrder;

import org.springframework.context.annotation.Bean;

import java.util.function.Consumer;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class Traceconsumer2Application {

	public static final Logger log = LoggerFactory.getLogger(Traceconsumer2Application.class);

	@Autowired
	private TraceRepository tRepository;

	@Autowired
        private  Environment env;

        @Autowired
        private TransportationOrderRepository tOrderRepository;

	public static void main(String[] args) {
		SpringApplication.run(Traceconsumer2Application.class, args);
	}

	@Bean("consumer")
	public Consumer<Trace> checkTrace() {
		return t -> {
			t.setTraceId(t.getTruck() + t.getLastSeen());
			tRepository.save(t);
			// String uri = env.getProperty("orders.server");
                        // RestTemplate restTemplate = new RestTemplate();
                        TransportationOrder result = null;
                        Optional<TransportationOrder> ot = tOrderRepository.findById(t.getTruck());
                        if (ot.isPresent()){
                                result = ot.get();
                        }                        
                        if (result != null && result.getSt() == 0) {
                                result.setLastDate(t.getLastSeen());
                                result.setLastLat(t.getLat());
                                result.setLastLong(t.getLng());
                                if (result.distanceToDestination() < 10)
                                        result.setSt(1);
                                tOrderRepository.save(result);
                                log.info("Order updated: "+ result);
                        }
		};
	};

}
